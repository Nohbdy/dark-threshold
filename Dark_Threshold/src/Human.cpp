#include "Human.h"
#include <ctime>
#include <random>
#include <glm/gtx/rotate_vector.hpp>

Human::Human() :
    _frames(0)
{
    //ctor
}

Human::~Human()
{
    //dtor
}

void Human::init(float speed, glm::vec2 pos)
{
    static std::mt19937 randomEngine(time(nullptr));
    static std::uniform_real_distribution<float> randDir(-1.0f, 1.0f);
    static std::uniform_real_distribution<float> randSkinShade(-45.0f, 45.0f);
    int skinShade = floor(randSkinShade(randomEngine));

    _health = 80.0f;

    _color.r = 210 + skinShade;
    _color.g = 160 + skinShade;
    _color.b = 150 + skinShade;
    _color.a = 255;

    _speed = speed;
    _position = pos;
    _direction = glm::vec2(randDir(randomEngine), randDir(randomEngine));
    if (_direction.length() == 0) _direction = glm::vec2(1.0f, 0.0f);

    _direction = glm::normalize(_direction);
}

void Human::update(const std::vector<std::string>& levelData,
                   std::vector<Human*>& humans,
                   std::vector<Zombie*>& zombies)
{
    ///CUSTOM, DEGREES TO RADIANS AND VICE VERSA
    const float DEG_TO_RAD = 3.14159265359f / 180.0f;
    const float RAD_TO_DEG = 180.0f / 3.14159265359f;

    static std::mt19937 randomEngine(time(nullptr));
    static std::uniform_real_distribution<float> randRotate(-40.0f * DEG_TO_RAD, 40.0f * DEG_TO_RAD);

    _position += _direction * _speed;

    //Randomly change direction every 20 frames
    if(_frames == 20)
    {
        _direction = glm::rotate(_direction, randRotate(randomEngine));
        _frames = 0;
    }
    else
    {
        _frames++;
    }

    if (collideWithLevel(levelData))
    {
        _direction = glm::rotate(_direction, randRotate(randomEngine));
    }

}
