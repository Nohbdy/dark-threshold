#include "Gun.h"
#include <random>
#include <ctime>
#include <glm/gtx/rotate_vector.hpp>

Gun::Gun(std::string name, int fireRate, int bulletsPerShot, float spread, float bulletDamage, float bulletSpeed, int bulletRange) :
    _name(name),
    _fireRate(fireRate),
    _bulletsPerShot(bulletsPerShot),
    _spread(spread),
    _bulletDamage(bulletDamage),
    _bulletSpeed(bulletSpeed),
    _bulletRange(bulletRange),
    _frameCounter(0)
{
    //ctor
}

Gun::~Gun()
{
    //dtor
}

void Gun::update(bool isMouseDown, const glm::vec2& position, const glm::vec2& direction, std::vector<Bullet>& bullets)
{
    _frameCounter++;
    if (_frameCounter >= _fireRate && isMouseDown)
    {
        fire(direction, position, bullets);
        _frameCounter = 0;
    }
    /*if (bullets.)
    {

    }*/
}

void Gun::fire(const glm::vec2& direction, const glm::vec2& position, std::vector<Bullet>& bullets)
{
    ///CUSTOM, DEGREES TO RADIANS AND VICE VERSA
    const float DEG_TO_RAD = 3.14159265359f / 180.0f;
    const float RAD_TO_DEG = 180.0f / 3.14159265359f;

    static std::mt19937 randomEngine(time(nullptr));
    static std::uniform_real_distribution<float> randRotate(-_spread * DEG_TO_RAD, _spread * DEG_TO_RAD);

    for(int i = 0; i < _bulletsPerShot; i++)
    {
        bullets.emplace_back(position,
                             glm::rotate(direction, randRotate(randomEngine)),
                             _bulletDamage,
                             _bulletSpeed);
    }
}
