#include "Level.h"
#include <ryEngine/Errors.h>
#include <ryEngine/ResourceManager.h>

#include <fstream>
#include <iostream>

Level::Level(const std::string& fileName)
{
    std::ifstream file;
    file.open(fileName);

    if (file.fail())
    {
        ryEngine::fatalError("Failed to open " + fileName);
    }

    // Throw away the first string in tmp
    std::string tmp;
    file >> tmp >> _numHumans;

    std::getline(file, tmp); // Throw away the rest of the first line

    // Read the level data
    while (std::getline(file, tmp))
    {
        _levelData.push_back(tmp);
    }
    file.close();

    _spriteBatch.init();
    _spriteBatch.begin();

    glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);
    ryEngine::Color whiteColor;
    whiteColor.setColor(255, 255, 255, 255);

    // Render all the tiles
    for (int y = 0; y < _levelData.size(); y++)
    {
        for (int x = 0; x < _levelData[y].size(); x++)
        {
            // Grab the tile
            char tile = _levelData[y][x];
            // Get dest rect
            glm::vec4 destRect(x * TILE_WIDTH, y * TILE_WIDTH, TILE_WIDTH, TILE_WIDTH);
            // Process the tile
            switch(tile)
            {
            case 'B':
            case 'R':
                _spriteBatch.draw(destRect,
                                  uvRect,
                                  ryEngine::ResourceManager::getTexture("Textures/red_bricks.png").id,
                                  0.0f,
                                  whiteColor);
                break;
            case 'G':
                _spriteBatch.draw(destRect,
                                  uvRect,
                                  ryEngine::ResourceManager::getTexture("Textures/vent.png").id,
                                  0.0f,
                                  whiteColor);
                break;
            case 'L':
                _spriteBatch.draw(destRect,
                                  uvRect,
                                  ryEngine::ResourceManager::getTexture("Textures/light_bricks.png").id,
                                  0.0f,
                                  whiteColor);
                break;
            case '@':
                _levelData[y][x] = '.'; // So we don't collide with a @
                _startPlayerPos.x = x * TILE_WIDTH;
                _startPlayerPos.y = y * TILE_WIDTH;
                _spriteBatch.draw(destRect,
                                  uvRect,
                                  ryEngine::ResourceManager::getTexture("Textures/manhole.png").id,
                                  0.0f,
                                  whiteColor);
                break;
            case 'Z':
                _levelData[y][x] = '.'; // So we don't collide with a Z
                _zombieStartPositions.emplace_back(x * TILE_WIDTH, y * TILE_WIDTH);
                _spriteBatch.draw(destRect,
                                  uvRect,
                                  ryEngine::ResourceManager::getTexture("Textures/manhole.png").id,
                                  0.0f,
                                  whiteColor);
                break;
            case '.':
                _spriteBatch.draw(destRect,
                                  uvRect,
                                  ryEngine::ResourceManager::getTexture("Textures/sidewalk.png").id,
                                  0.0f,
                                  whiteColor);
                break;
            default:
                std::printf("Unexpected symbol %c at(%d,%d)\n", tile, x, y);
                break;
            }
        }
    }

    _spriteBatch.end();
}

Level::~Level()
{
    //dtor
}

void Level::draw()
{
    _spriteBatch.renderBatch();
}
