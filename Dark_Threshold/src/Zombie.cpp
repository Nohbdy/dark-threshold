#include "Zombie.h"
#include "Human.h"

Zombie::Zombie()
{
    //ctor
}

Zombie::~Zombie()
{
    //dtor
}

void Zombie::init(float speed, glm::vec2 pos)
{
    static std::mt19937 randomEngine(time(nullptr));
    static std::uniform_real_distribution<float> randHealth(60.0f, 220.0f);
    static std::uniform_real_distribution<float> randSkinShade(-45.0f, 45.0f);
    int skinShade = floor(randSkinShade(randomEngine));

    _speed    = speed;
    _position = pos;
    _health   = randHealth(randomEngine);

    _color.r = 0;
    _color.g = 160 + skinShade;
    _color.b = 20;
    _color.a = 255;
}

void Zombie::update(const std::vector<std::string>& levelData,
                    std::vector<Human*>& humans,
                    std::vector<Zombie*>& zombies)
{
    Human* closestHuman = getNearestHuman(humans);

    if (closestHuman != nullptr)
    {
        glm::vec2 direction = glm::normalize(closestHuman->getPosition() - _position);
        _position += direction * _speed;
    }


    collideWithLevel(levelData);
}

Human* Zombie::getNearestHuman(std::vector<Human*>& humans)
{
    Human* closestHuman = nullptr;
    float smallestDistance = 9999999.0f;

    for(int i = 0; i < humans.size(); i++)
    {
        glm::vec2 distVec = humans[i]->getPosition() - _position;
        float distance = glm::length(distVec);

        if (distance < smallestDistance)
        {
            smallestDistance = distance;
            closestHuman = humans[i];
        }
    }

    return closestHuman;
}
