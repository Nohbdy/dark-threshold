#include "MainGame.h"

#include <ryEngine/ryEngine.h>
#include <ryEngine/Timing.h>
#include <ryEngine/Errors.h>
#include <random>
#include <ctime>

#include <SDL2/SDL.h>
#include <iostream>

#include "Zombie.h"
#include "Gun.h"

const float HUMAN_SPEED = 1.0f;
const float ZOMBIE_SPEED = 1.2f;

MainGame::MainGame() :
    _screenWidth(1024),
    _screenHeight(768),
    _gameState(GameState::PLAY),
    _fps(0),
    _player(nullptr),
    _numHumansKilled(0),
    _numZombiesKilled(0)
{
    // Empty
}

MainGame::~MainGame()
{
    for(unsigned int i = 0; i < _levels.size(); i++)
    {
        delete _levels[i];
    }
}

void MainGame::run()
{
    std::printf("\nLoading...\n");
    initSystems();
    initLevel();

    std::printf("\n..Done!\n\n");
    std::printf("[HOW TO PLAY]\nUse WASD keys to move.\nHover mouse to aim, click to shoot.\nSwitch between weapons using numbers 1-9.");
    std::printf("\n1: Magnum\n2: Shotgun\n3: MP5\n4: 4444\n5: 5555\n6: 6666\n 7: 7777\n 8: 8888\n9: 9999");
    gameLoop();
}

void MainGame::initSystems()
{
    ryEngine::init();

    _window.create("Dark Threshold v0.2", _screenWidth, _screenHeight, 0);
    glClearColor(0.5f, 0.5f, 0.6f, 1.0f);

    initShaders();

    _agentSpriteBatch.init();

    _camera.init(_screenWidth, _screenHeight);

}

void MainGame::initLevel()
{
    // Level 1
    _levels.push_back(new Level("Levels/level1.txt"));
    _currentLevel = 0;

    _player = new Player();
    _player->init(7.0f, _levels[_currentLevel]->getStartPlayerPos(), &_inputManager, &_camera, &_bullets);

    _humans.push_back(_player);

    static std::mt19937 randomEngine;
    randomEngine.seed(time(nullptr));
    static std::uniform_int_distribution<int> randX(2, _levels[_currentLevel]->getWidth() - 2);
    static std::uniform_int_distribution<int> randY(2, _levels[_currentLevel]->getHeight() - 2);

    // Add all the random humans
    for (int i = 0; i < _levels[_currentLevel]->getNumHumans(); i++)
    {
        _humans.push_back(new Human);
        glm::vec2 pos(randX(randomEngine) * TILE_WIDTH, randY(randomEngine) * TILE_WIDTH);
        _humans.back()->init(HUMAN_SPEED, pos);
    }

    //Add the zombies
    const std::vector<glm::vec2>& zombiePositions = _levels[_currentLevel]->getZombieStartPositions();
    for (int i = 0; i < zombiePositions.size(); i++)
    {
        _zombies.push_back(new Zombie);
        glm::vec2 pos(randX(randomEngine) * TILE_WIDTH, randY(randomEngine) * TILE_WIDTH);
        _zombies.back()->init(ZOMBIE_SPEED, zombiePositions[i]);
    }

    // Set up the player guns
    _player->addGun(new Gun("Pistolio",       32,      1,      5.0f,    52.0f,    20.0f,     300));
    _player->addGun(new Gun("Spatgon",        26,     16,     11.0f,    15.0f,    25.0f,     300));
    _player->addGun(new Gun("Brrraptor",       8,      1,     15.0f,    25.0f,    22.0f,     300));
    _player->addGun(new Gun("Snipshaw",      100,      1,      1.0f,    80.0f,    28.0f,     300));
    _player->addGun(new Gun("Spewlore",        2,      2,     35.0f,    15.0f,     4.0f,     100));
    _player->addGun(new Gun("Splashnok",       3,      3,    120.0f,    25.0f,    12.0f,     300));
    _player->addGun(new Gun("Sploderra",     140,     30,    180.0f,   130.0f,     7.0f,     300));
    _player->addGun(new Gun("XyXyO",          15,      9,     50.0f,    35.0f,    22.0f,     300));
    _player->addGun(new Gun("Rigshut",        36,      10,     2.0f,    80.0f,    12.0f,     300));
    ///-----------------------name---------fireRate--#shot----spread----damage---bulSpeed---range---/// (range not-yet-used) ///

}

void MainGame::initShaders()
{
    // Compile our color shader
    _textureProgram.compileShaders("Shaders/textureShading.vert", "Shaders/textureShading.frag");
    _textureProgram.addAttribute("vertexPosition");
    _textureProgram.addAttribute("vertexColor");
    _textureProgram.addAttribute("vertexUV");
    _textureProgram.linkShaders();
}

void MainGame::gameLoop()
{
    ryEngine::FpsLimiter fpsLimiter;
    fpsLimiter.setMaxFPS(60.0f);

    while(_gameState == GameState::PLAY)
    {
        fpsLimiter.begin();

        checkVictory();

        processInput();

        updateAgents();

        updateBullets();

        _camera.setPosition(_player->getPosition());

        _camera.update();

        drawGame();

        _fps = fpsLimiter.end();
    }
}

void MainGame::updateAgents()
{
    // Update all humans
    for (int i = 0; i < _humans.size(); i++)
    {
        _humans[i]->update(_levels[_currentLevel]->getLevelData(),
                           _humans,
                           _zombies);
    }

    // Update all zombies
    for (int i = 0; i < _zombies.size(); i++)
    {
        _zombies[i]->update(_levels[_currentLevel]->getLevelData(),
                           _humans,
                           _zombies);
    }

    //Update Zombie collisions
    for (int i = 0; i < _zombies.size(); i++)
    {
        // Collide with other zombies
        for (int j = i + 1; j < _zombies.size(); j++)
        {
            _zombies[i]->collideWithAgent(_zombies[j]);
        }
        // Collide with humans
        for (int j = 1; j < _humans.size(); j++)
        {
            if (_zombies[i]->collideWithAgent(_humans[j]))
            {
                _zombies.push_back(new Zombie);
                _zombies.back()->init(ZOMBIE_SPEED, _humans[j]->getPosition());
                //Delete the human
                delete _humans[j];
                _humans[j] = _humans.back();
                _humans.pop_back();
                std::printf("!");
            }
        }

        // Collide with player
        if (_zombies[i]->collideWithAgent(_player))
        {
            ryEngine::fatalError("\nYOU LOSE");
        }
    }

    //Update Human collisions
    for (int i = 0; i < _humans.size(); i++)
    {
        // Collide with other humans
        for (int j = i + 1; j < _humans.size(); j++)
        {
            _humans[i]->collideWithAgent(_humans[j]);
        }
    }

    // Update all zombies
}

void MainGame::updateBullets()
{
    // Update and collide with world
    for(int i = 0; i < _bullets.size();)
    {
        // If update returns true, teh bullet collided with a wall
        if (_bullets[i].update(_levels[_currentLevel]->getLevelData()))
        {
            _bullets[i] = _bullets.back();
            _bullets.pop_back();
        }
        else
        {
             i++;
        }
    }

    bool wasBulletRemoved;

    // Collide with humans and zombies
    for (int i = 0; i < _bullets.size(); i++)
    {
        wasBulletRemoved = false;
        // Loop through zombies
        for(int j = 0; j < _zombies.size(); )
        {
            // Check collision
            if(_bullets[i].collideWithAgent(_zombies[j]))
            {
                // Damage zombie and kill it if it's out of health
                if (_zombies[j]->applyDamage(_bullets[i].getDamage()))
                {
                    delete _zombies[j];
                    _zombies[j] = _zombies.back();
                    _zombies.pop_back();
                    std::printf("$");
                    _numZombiesKilled++;
                }
                else
                {
                    j++;
                }
                // Remove the bullet
                _bullets[i] = _bullets.back();
                _bullets.pop_back();
                wasBulletRemoved = true;
                i--; // Make sure we don't skip a bullet
                //Since the bullet died, no need to loop through any more zombies
                break;
            }
            else
            {
                 j++;
            }
        }
        // Loop through humans
        if(wasBulletRemoved == false)
        {

            for(int j = 1; j < _humans.size(); )
            {
                // Check collision
                if(_bullets[i].collideWithAgent(_humans[j]))
                {
                    // Damage human and kill it if it's out of health
                    if (_humans[j]->applyDamage(_bullets[i].getDamage()))
                    {
                        delete _humans[j];
                        _humans[j] = _humans.back();
                        _humans.pop_back();
                        std::printf("X");
                        _numHumansKilled++;
                    }
                    else
                    {
                        j++;
                    }
                    // Remove the bullet
                    _bullets[i] = _bullets.back();
                    _bullets.pop_back();
                    wasBulletRemoved = true;
                    i--; // Make sure we don't skip a bullet
                    //Since the bullet died, no need to loop through any more humans
                    break;
                }
                else
                {
                     j++;
                }
            }
        }
    }
}

void MainGame::checkVictory()
{
    // If all zombies are dead we win
    if(_zombies.empty())
    {
        // TODO: Support for multiple levels
        std::printf("\n*** You win! ***\n You killed %d humans and %d zombies. There are %d/%d civilians remaining.",
                    _numHumansKilled, _numZombiesKilled, _humans.size() - 1, _levels[_currentLevel]->getNumHumans());
        ryEngine::fatalError("");
    }
}

void MainGame::processInput()
{
    SDL_Event evnt;
    //Will keep looping until there are no more events to process
    while (SDL_PollEvent(&evnt))
    {
        switch (evnt.type)
        {
        case SDL_QUIT:
            _gameState = GameState::EXIT;
            break;
        case SDL_MOUSEMOTION:
            _inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
            break;
        case SDL_KEYDOWN:
            _inputManager.pressKey(evnt.key.keysym.sym);
            break;
        case SDL_KEYUP:
            _inputManager.releaseKey(evnt.key.keysym.sym);
            break;
        case SDL_MOUSEBUTTONDOWN:
            _inputManager.pressKey(evnt.button.button);
            break;
        case SDL_MOUSEBUTTONUP:
            _inputManager.releaseKey(evnt.button.button);
            break;
        }
    }
}

void MainGame::drawGame()
{
    // Set the base depth to 1.0
    glClearDepth(1.0);
    // Clear the color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _textureProgram.use();

    glActiveTexture(GL_TEXTURE0);

    // Make sure the shader uses texture
    GLint textureUniform = _textureProgram.getUniformLocation("mySampler");
    glUniform1i(textureUniform, 0);

    glm::mat4 projectionMatrix = _camera.getCameraMatrix();
    GLint pUniform = _textureProgram.getUniformLocation("P");
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projectionMatrix[0][0]);

    // Draw level
    _levels[_currentLevel]->draw();

    // Begin drawing agents
    _agentSpriteBatch.begin();

    //Draw the humans
    for(unsigned int i = 0; i < _humans.size(); i++)
    {
        //_levels[_currentLevel]->draw();
        _humans[i]->draw(_agentSpriteBatch);
    }

    //Draw the zombies
    for(unsigned int i = 0; i < _zombies.size(); i++)
    {
        //_levels[_currentLevel]->draw();
        _zombies[i]->draw(_agentSpriteBatch);
    }

    // Draw the bullets
    for(int i = 0; i < _bullets.size(); i++)
    {
        _bullets[i].draw(_agentSpriteBatch);
    }

    _agentSpriteBatch.end();

    _agentSpriteBatch.renderBatch();

    _textureProgram.unuse();

    // Swap our buffer and draw everything to the screen!
    _window.swapBuffer();
}
