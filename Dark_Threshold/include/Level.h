#ifndef LEVEL_H
#define LEVEL_H

#include <string>
#include <vector>

#include <ryEngine/SpriteBatch.h>

const int TILE_WIDTH = 64;

class Level
{
public:
    // Load the Level
    Level(const std::string& fileName);
    ~Level();

    void draw();

    // Getters
    int getWidth() const { return _levelData[0].size(); }
    int getHeight() const { return _levelData.size(); }
    int getNumHumans() const { return _numHumans; }
    const std::vector<std::string>& getLevelData() const { return _levelData; }
    glm::vec2 getStartPlayerPos() const { return _startPlayerPos; }
    const std::vector<glm::vec2>& getZombieStartPositions() const { return _zombieStartPositions; }


private:
    std::vector<std::string> _levelData;
    int _numHumans;
    ryEngine::SpriteBatch _spriteBatch;

    glm::vec2 _startPlayerPos;
    std::vector<glm::vec2> _zombieStartPositions;

};

#endif // LEVEL_H
