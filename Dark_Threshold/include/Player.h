#ifndef PLAYER_H
#define PLAYER_H

#include "Human.h"
#include <ryEngine/InputManager.h>
#include <ryEngine/Camera2D.h>

class Gun;
class Bullet;

class Player : public Human
{
    public:
        Player();
        ~Player();

        void init(float speed, glm::vec2 pos, ryEngine::InputManager* inputManager, ryEngine::Camera2D* camera, std::vector<Bullet>* bullets);

        void addGun(Gun* gun);

        void update(const std::vector<std::string>& levelData,
                    std::vector<Human*>& humans,
                    std::vector<Zombie*>& zombies) override;
    private:
        ryEngine::InputManager* _inputManager;

        std::vector<Gun*> _guns;
        int _currentGunIndex;

        ryEngine::Camera2D* _camera;
        std::vector<Bullet>* _bullets;
};

#endif // PLAYER_H
