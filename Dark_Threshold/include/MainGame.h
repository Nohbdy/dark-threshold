// Made a change! (test/example)
#ifndef MAINGAME_H
#define MAINGAME_H

#include <ryEngine/Window.h>
#include <ryEngine/GLSLProgram.h>
#include <ryEngine/Camera2D.h>
#include <ryEngine/InputManager.h>
#include <ryEngine/SpriteBatch.h>

#include "Level.h"
#include "Player.h"
#include "Bullet.h"
#include "Config.h"

class Zombie;

enum class GameState
{
    PLAY,
    EXIT
};

class MainGame
{
public:
    MainGame();
    ~MainGame();

    /// Runs the game
    void run();

private:
    /// Initializes the core systems
    void initSystems();

    /// Initializes the level and sets up everything
    void initLevel();

    /// Initializes the shaders
    void initShaders();

    /// Main game loop for the program
    void gameLoop();

    /// Update agents
    void updateAgents();

    /// Update bullets
    void updateBullets();

    ///Checks the victory condition
    void checkVictory();

    /// Handles input processing
    void processInput(); ///< Vector of all humans.

    /// Renders the game
    void drawGame();

    /// Member Variables
    ryEngine::Window _window; ///< The game window

    ryEngine::GLSLProgram _textureProgram; ///< The shader program

    ryEngine::InputManager _inputManager; ///< Handles input

    ryEngine::Camera2D _camera; ///< Main Camera

    ryEngine::SpriteBatch _agentSpriteBatch;

    std::vector<Level*> _levels; ///< Vector of all levels

    int _screenWidth, _screenHeight;

    GameState _gameState;

    float _fps;

    int _currentLevel;

    Player* _player;
    std::vector<Human*> _humans; ///< Vector of all humans.
    std::vector<Zombie*> _zombies; ///< Vector of all zombies.
    std::vector<Bullet> _bullets;

    int _numHumansKilled; // Humans killed by player
    int _numZombiesKilled;
};

#endif // MAINGAME_H
