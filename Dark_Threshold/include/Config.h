#ifndef CONFIG_H
#define CONFIG_H

#include <fstream>
#include <iostream>
#include <string>

#include <ryEngine/Errors.h>

class Config
{
public:
    /** Default constructor */
    Config();
    /** Default destructor */
    ~Config();

    void load(const std::string& fileName);

private:
};

#endif // CONFIG_H
