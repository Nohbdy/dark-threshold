Dark Threshold is currently a top-down shooter in which you shoot zombies and
avoid shooting humans.

All code is nearly identical to the tutorial from the "Zombie Game" made by
Benjamin Arnold under the programming tutorial series "Making Games With Ben".

Once core concepts of C++, game development, engine development, and all other
related skillsets reach some unknown level of understanding and ability, Dark
Threshold will likely transform as a game - likely no longer being a zombie
game nor a top-down shooter.

The project as a tutorial has already taught much, and eventually it is hoped
it will act as a decent source of learning for GPDMCC - the Game Programming &
Development club) - for which I founded.

**UPDATE 2020-02-14**: May be working on this or another project soon!